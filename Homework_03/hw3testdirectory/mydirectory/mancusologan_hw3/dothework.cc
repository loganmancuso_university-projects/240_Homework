#include "dothework.h"

/******************************************************************************
 *3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789
 * Homework 3 'DoTheWork' application class.
 *
 * Author: Duncan A. Buell
 * Used with permission and modified by: Mancuso Logan
 * Date last modified: 10 September 2016
**/


/*
 * this program will do the following:
 * using the ReadData from main.cc Scanner, take each line and assign to new
 * string and pass to record.cc to be evaluated and stored
 * it will also take and sort the information stored in vector "the_data_"
 * the medthod tostring will string together the values in the_data_[i]
 * the swap function swapps i and j within the_data_
 * tostringbyname sorts by name and prints in the order
 * tostringbynumber sorts by number and prints in the order
 * 
*/

static const string kTag = "DoTheWork: ";

/******************************************************************************
 * Constructor
**/
DoTheWork::DoTheWork() {
}

/******************************************************************************
 * Destructor
**/
DoTheWork::~DoTheWork() {
}

/******************************************************************************
 * Accessors and Mutators
**/

/******************************************************************************
 * General functions.
**/

/******************************************************************************
 * Function 'ReadData'.
 * We read data from the input stream, record by record, and push it into
 * the 'vector' named 'the_data_'.
 *
 * NOTE: We are assuming that if there is any data, there is an entire record,
 * so we can just read the entire record as a unit.
 *
 * Parameter:
 *   data_stream -- the input data stream
**/
void DoTheWork::ReadData(Scanner& data_stream)
{

#ifdef EBUG
  cout << kTag << "enter ReadData\n"; 
#endif
  //read data and input to vector of data type record

  while( data_stream.HasNext() ) {
    Record record = Record();
    string line = data_stream.NextLine();
    record = record.ReadData(line); //pass data to record to parse
    the_data_.push_back(record); //add data to vector the_data_
  }
//print to console so I dont have to check zoutfile everytime
  cout << "RAW DATA " << endl;
  string raw = ToString();
  cout << raw << endl;

  cout << "BY NAME " << endl;
  string name = ToStringByName();
  cout << name  << endl;

  cout << "BY NUMBER " << endl;
  string number = ToStringByNumber();
  cout << ToStringByNumber() << endl;

#ifdef EBUG
  cout << kTag << "leave ReadData\n"; 
#endif
} // void DoTheWork::ReadData(Scanner& data_stream)

/******************************************************************************
 * Function 'SortRecords'.
 * We sort the records either by 'surname_' or by 'phone_number_'.
 *
 * This is a simple bubble sort.
 *
 * Parameter:
 *   how_sort -- either the string 'name' or 'number'
**/
void DoTheWork::SortRecords(string how_sort) {
#ifdef EBUG
  cout << kTag << "enter SortRecords\n"; 
#endif

  string name = "name";
  string number = "number";
  bool swap;
  int size = the_data_.size();

  swap = true;
  while( swap ) {
    swap = false;
    for( int i = 0; i < (size-1); ++i ) {
      if ( how_sort == name ) {
        if ( the_data_[i].CompareName(the_data_[i+1]) == true ) {
          Swap( i, (i+1) ); //swap
          swap = true;
        } //end if
      }//end if name
      if ( how_sort == number ) {
        if ( the_data_[i].CompareNumber(the_data_[i+1]) == true ) {
          Swap( i, (i+1) ); //swap
          swap = true;
        } //end if
      }//end if number
    }//end for loop
  }//end while


#ifdef EBUG
  cout << kTag << "leave SortRecords\n"; 
#endif
} // void DoTheWork::SortRecords(string howSort)

/******************************************************************************
 * Swap function, swaps subscripts 'i' and 'j' data in 'the_data_'
**/
void DoTheWork::Swap(int i, int j) {

  Record temp_swap = Record();
  temp_swap = the_data_[j];
  the_data_[j] = the_data_[i];
  the_data_[i] = temp_swap;

} // void Swap(int i, int j) {

/******************************************************************************
 * Function 'ToString'.
 * This is the usual 'ToString'. We output the 'vector' of 'Record'
 * instances without regard to the order.
 *
 * Returns:
 *   a formatted 'string' version of the 'vector' of records
**/
string DoTheWork::ToString() {
  string s = "";
  int size = the_data_.size();
#ifdef EBUG
  cout << kTag << "enter ToString\n"; 
#endif

  for( int i = 0; i < (size-1); ++i ) {
    s += the_data_[i].ToString() + "\n";
  }

#ifdef EBUG
  cout << kTag << "leave ToString\n"; 
#endif
  return s;
} // void DoTheWork::ToString()

/******************************************************************************
 * Function 'ToStringByName'.
 *
 * First we sort the data by name, then we call the regular 'ToString'.
 *
 * Returns:
 *   a string of the records output in sorted order by name
**/
string DoTheWork::ToStringByName() {

#ifdef EBUG
  cout << kTag << "enter ToStringByName\n"; 
#endif

  SortRecords( "name" );
  ToString();

#ifdef EBUG
  cout << kTag << "leave ToStringByName\n"; 
#endif

return this->ToString();
} // void DoTheWork::ToStringByName()

/******************************************************************************
 * Function 'ToStringByNumber'.
 *
 * First we sort the data by number, then we call the regular 'ToString'.
 *
 * Returns:
 *   a string of the records output in sorted order by number
**/
string DoTheWork::ToStringByNumber() {

#ifdef EBUG
  cout << kTag << "enter ToStringByNumber\n"; 
#endif

 SortRecords( "number" );
 ToString();

#ifdef EBUG
  cout << kTag << "leave ToStringByNumber\n"; 
#endif

return this->ToString();
} // void DoTheWork::ToStringByNumber()

