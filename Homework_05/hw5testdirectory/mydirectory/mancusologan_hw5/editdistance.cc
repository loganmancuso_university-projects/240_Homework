#include "editdistance.h"

/******************************************************************************
 *3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789
 * Class 'EditDistance' for the edit distance computation.
 *
 * Author: Duncan A. Buell
 * Modified by Mancuso Logan
 * Date last modified: 4 October 2016
**/

/******************************************************************************
 * Constructor
**/
EditDistance::EditDistance() {
}

/******************************************************************************
 * Destructor
**/
EditDistance::~EditDistance() {
}

/******************************************************************************
 * Accessors and Mutators
**/

/******************************************************************************
 * General functions.
**/

/******************************************************************************
 * Function 'ComputeDistances'.
 * This function computes the distances between the two sentences.
 * builds the first substring lines for the first line and column
 * some string 1 = abcd
 * some string 2 = abzde
 * XX	a	b	c	d	XX
 * XX	0	1	2	3	4
 * a	1
 * b	2
 * z	3
 * d	4
 * e	5
 * once this is built using the values top, left and diagonal compute missing
 * take the minimum of all 3 if from != to and they are not a dummy blank XX
 * if they are not a blank XX then diagonal is a substitution so +2
**/
void EditDistance::ComputeDistances() {
  //build first line and column of distances
  for(int i = 0; i < length_; ++i) {
    vector<int> tmp;
    for (int j = i; j < length_+i; ++j) {
      tmp.push_back(j);
    }//end for
    dist_.push_back(tmp);
  }//end for
  for (int i = 1; i < length_; ++i) {
    string to = sentence2_.at(i);
    for (int j = 1; j < length_; ++j) {
      string from = sentence1_.at(j);
      int top = 1 + dist_.at(i-1).at(j);
      int left = 1 + dist_.at(i).at(j-1);
      int diag = dist_.at(i-1).at(j-1);
      if (from != to) {
        //if either are blank then it needs to be inserted not substituted
        if (from == kDummyString_ || to == kDummyString_) {
          ++diag;
        }//end if
        else {
        //if neither are blank than it needs to be substituted
          diag += 2;
        }//end else
      }//end if
      //place minimum of 3 values at bottom right corner
      dist_.at(i).at(j) = FindMin(top,left,diag);
    }//end for
  }//end for
}//end function

/******************************************************************************
 * Function 'FindMin'.
 * takes in three values and computes the minimum returning the int
 * value of the minimum of the 3
**/
int EditDistance::FindMin(int top, int left, int diag) {
  int z = min(top,left); //min of first two
  return min(z,diag); //min of previous minimum comparison
}

/******************************************************************************
 * Function 'Initialize'.
 * initializes the vectors sentence1_ and sentence2_
 * takes the line from the file and sends to function that returns a vector
 * if its the first line assign to that vector
 * else assign to the second vector
 * from this make them the same length by calling the function
 * from this call computation functions
**/
void EditDistance::Initialize(Scanner& data_stream) {
  ScanLine scanline;
  int number_of_lines = 2;
  for (int i = 0; i < number_of_lines; ++i) {
    string file_line = data_stream.NextLine();
    if (i == 0) { //if first line
      sentence1_ = StringReader(file_line);
    }//end if
    if (i == 1) { //if second line
      sentence2_ = StringReader(file_line);
    }//end if
  }//end for loop
  MakeSameLength();
}//end function

/******************************************************************************
 * Function 'ToString'.
 * Returns:
 *   a 'string' of a vector concatinated with all of the elements
 * | XX | elements | elements | may end in XX if the shorter of the two vectors
**/
string EditDistance::ToString(vector<string> vector) {
  string s = "| ";
  int size = vector.size();
  for(int i = 0; i < size; ++i) {
    s += vector.at(i);
    s += " | ";
  }//end for
  s += "\n";
  return s;
}//end function

/******************************************************************************
 * Function 'StringReader'.
 *
 * takes in a string called input, using the ScanLine, read through the line
 * takes each work delim by while space and pushes to a vector called
 * return_vector, when done returns the vector
**/
vector<string> EditDistance::StringReader(const string& input) {
  ScanLine scanline;
  vector<string> return_vector;
  return_vector.push_back(kDummyString_); //begin with "XX"
  scanline.OpenString(input);
  while (scanline.HasNext()) {
    string token = scanline.Next(); //each word in the line
    return_vector.push_back(token); //pushed onto vector
  }//end while
  return return_vector;
}//end function

/*****************************************************************************
 * Function 'Print'
 * uses the ToString to print both vectors and the matrix with the edit
 * distance and returns a string to be printed to file
**/
string EditDistance::Print(){
  string s;
  s += "Sentence 1 \n";
  s += ToString(sentence1_);
  s += "Sentence 2 \n";
  s += ToString(sentence2_);
  s += "Length: ";
  s += to_string(length_);
  s += "\nDistance Matrix:\n";
  s += PrintMatrix();
  return s;
}//end function

/*****************************************************************************
 * Function 'MakeSameLength'
 * makes sentence1_ and sentence2_ the same length by adding dummy string "XX"
 * simple if statement to see which is longer, iff longer add XX else dont
**/
void EditDistance::MakeSameLength() {
  int length_of_1 = sentence1_.size();
  int length_of_2 = sentence2_.size();
  if (length_of_1 > length_of_2) { //add more to 2
    int diff = length_of_1 - length_of_2; //difference
    for (int i = 0; i < diff; i++) {
      sentence2_.push_back(kDummyString_); //add "XX"
      ++length_of_2;
    }//end for
  }//end if
  else if (length_of_1 < length_of_2) { //add more to 1
    int diff = length_of_2 - length_of_1; //difference
    for (int i = 0; i < diff; i++) {
      sentence1_.push_back(kDummyString_); //add "XX"
      ++length_of_1;
    }//end for
  }//end if
  if (length_of_1 == length_of_2) {
    length_ = length_of_1; //assign length variable to now common length
  }//end if
}//end function

/******************************************************************************
 * Function 'PrintMatrix'
 * prints the integer matrix of the two vectors
**/
string EditDistance::PrintMatrix() {
  string s;
  //integer to control the spaces between the values when printed
  int spaces = 9;
  s += Utils::Format("",spaces);
  for (int i = 0; i < length_; ++i) {
    s += Utils::Format(sentence1_.at(i),spaces);
  }//end for
  s += "\n";
  for (int vertical = 0; vertical < length_; ++vertical) {
      s += Utils::Format(sentence2_.at(vertical),spaces);
    for (int horizontal = 0; horizontal < length_; ++horizontal) {
      s += Utils::Format(to_string(dist_.at(vertical).at(horizontal)),spaces);
    }//end for horizontal
    s += "\n";
  }//end for vertical
  return s;
}//end function
