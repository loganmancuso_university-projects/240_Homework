/****************************************************************
 * Header file for the application class for the edit distance
 * program.
 *
 * Author/copyright:  Duncan Buell
 * Modified by Mancuso Logan
 * Date last modified: 4 October 2016
 *
**/

#ifndef EDITDISTANCE_H
#define EDITDISTANCE_H

#include <iostream>
#include <vector>
using namespace std;

#include "../../Utilities/scanner.h"
#include "../../Utilities/scanline.h"
#include "../../Utilities/utils.h"

class EditDistance {
public:
 EditDistance();
 virtual ~EditDistance();
//functions
 void ComputeDistances();
 int FindMin(int a, int b, int c); //min helper
 void Initialize(Scanner& scanner);
 void MakeSameLength(); //makes strings 1 and 2 same length by "padding" shorter one
 string Print(); //print method
 string PrintMatrix(); //print the distance matrix function, called by Print()
 vector<string> StringReader(const string& string); //reads string from file
 string ToString(const vector<string> vector); //convert vector to string

private:
 const int kCostDeletion_ = 1;
 const int kCostInsertion_ = 1;
 const int kCostSubstitution_ = 2;
 const string kDummyString_ = "XX";

 // we will pad the shorter string to get a common 'length_'
 int length_;

 // note the space in '> >' because the compiler needs it
 vector<vector<int> > dist_;
 vector<string> sentence1_;
 vector<string> sentence2_;
};

#endif
