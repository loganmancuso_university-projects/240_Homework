/****************************************************************
 * Header file for the class to assemble packets into messages.
 *
 * Author/copyright:  Duncan Buell
 * Date: 8 August 2016
 * Edited By Mancuso Logan
 * Last Edit Date: 10/24/2016 14:31:17
 *
**/

#ifndef PACKETASSEMBLER_H
#define PACKETASSEMBLER_H

#include <iostream>
#include <map>
using namespace std;

#include "message.h"

#include "../../Utilities/scanner.h"
#include "../../Utilities/scanline.h"

class PacketAssembler {
public:
 PacketAssembler();
 virtual ~PacketAssembler();

 void RunForever(Scanner& scanner, ofstream& out_stream);

private:
 map<int, Message> messages_map_;

 bool MessagesMapContains(int messageID) const;
 string DumpMessage(int messageID);
 void ReadPacket(string& input, ofstream& out_stream);

};

#endif
