/****************************************************************
 * Header file for a single message.
 *
 * Author/copyright:  Duncan Buell
 * Date: 8 August 2016
 * Edited By Mancuso Logan
 * Last Edit Date: 10/24/2016 14:31:17
 *
**/

#ifndef MESSAGE_H
#define MESSAGE_H

#include <iostream>
#include <map>
using namespace std;

#include "../../Utilities/scanner.h"
#include "../../Utilities/scanline.h"

#include "packet.h"

class Message {
public:
 Message();
 virtual ~Message();

 bool Contains(const Packet& p) const;
 int GetMessageID() const;
 void Insert(const Packet& p);
 bool IsComplete() const;

 string ToString() const;

 friend ostream& operator <<(ostream& out_stream, const Message& msg);

private:
 int messageID_ = Packet::kDummyMessageID_;
 int packet_count_ = Packet::kDummyPacketCount_;
 map<int, Packet> message_;
};

#endif
