#include "packet.h"

/******************************************************************************
 *3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789
 * Class 'Packet' for a single packet.
 *
 * This is simply a data payload class.
 * Author: Duncan A. Buell
 * Date last modified: 4 November 2014
 * Edited By Mancuso Logan
 * Last Edit Date: 10/24/2016 14:31:17
**/

/******************************************************************************
 * Constructor
**/
Packet::Packet() {
}

/******************************************************************************
 * Constructor with data supplied.
**/
Packet::Packet(Scanner& scanner) {
}

/******************************************************************************
 * Destructor
**/
Packet::~Packet() {
}

/******************************************************************************
 * Accessors and Mutators
**/

/******************************************************************************
 * Accessor 'GetMessageID'.
 *
 * Returns:
 *   the 'messageID'
**/
int Packet::GetMessageID() const {
  return messageID_;
}

/******************************************************************************
 * Accessor 'GetPacketCount'.
 *
 * Returns:
 *   the 'packet_count_'
**/
int Packet::GetPacketCount() const {
  return packet_count_;
}

/******************************************************************************
 * Accessor 'GetPacketID'.
 *
 * Returns:
 *   the 'packetID_'
**/
int Packet::GetPacketID() const {
  return packetID_;
}

/******************************************************************************
 * Accessor 'GetPayload'.
 *
 * Returns:
 *   the 'payload_'
**/
string Packet::GetPayload() const {
  return payload_;
}

/******************************************************************************
 * General functions.
**/

/******************************************************************************
 * Method to test two packets for equality.
 * messageID_ is unique to the message
 * packetID_ is unique to the packet
 * both need to be satisfied to have a unique packet in a unique message
**/
bool Packet::Equals(const Packet& that) const {
  bool return_value = false;
  if (messageID_ == that.GetMessageID() && packetID_ == that.GetPacketID()) {
    return_value = true;
  }//end if
  return return_value;
}

/******************************************************************************
 * Method to read a packet.
 * <     0      ,    1      ,     2         ,     3....>
 * <(messageID_),(packetID_),(packet_count_),(message_)>
 * this method is passed a vector with the known constructs above
**/
void Packet::ReadPacket(vector<string>& input_line) {
  int length = input_line.size();
  messageID_ = std::stoi(input_line.at(0));
  packetID_ = std::stoi(input_line.at(1));
  packet_count_ = std::stoi(input_line.at(2));
  //read the final elements in the vector and make a string
  for (int i = 3; i < length; ++i) {
    payload_ += " ";
    payload_ += input_line.at(i);
  }//end for

}

/******************************************************************************
 * Function 'ToString'.
 *
 * Returns:
 *   the 'ToString' of the class
 * string will be returned as a single line starting with
 * messageID_ then packetID_, packet_count_ and then the payload_
**/
string Packet::ToString() const {
  string s = "";
  int spaces = 6;
  s += Utils::Format(to_string(GetMessageID()),spaces);
  s += Utils::Format(to_string(GetPacketID()),spaces);
  s += Utils::Format(to_string(GetPacketCount()),spaces);
  s += Utils::Format(GetPayload(),spaces);
  return s;
}
