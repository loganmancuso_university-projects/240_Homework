#include "main.h"
/****************************************************************
 * 'main.cc'
 * Main file for program
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 12-01-2016--10:20:07
 * THIS IS A TEST FILE CHANGES WILL NOT BE SAVED
 *
**/

static const string kTag = "Main: ";

int main(int argc, char *argv[]) {
  //variables for files
  string config_filename = "XX";
  string pct_filename = "XX";
  string out_filename = "XX";
  string log_filename = "XX";
  string outstring = "XX";
  string timecall_output = "XX";
  //setup an outstream for data to write to log file
  ofstream out_stream;
  //scanner is used for file input, reading and passing data
  Scanner config_stream;
  Scanner pct_stream;

  Configuration config;
  Simulation simulation;
  //random number generator
  MyRandom random;


  cout << kTag << "Beginning Execution" << endl;
  /*******************************************************
   * command line arguments are formatted as follows:
   * Aprog <configfile> <pctfile> <outfile> <logfile>
  **/
  Utils::CheckArgs(4, argc, argv, "configfilename pctfilename outfilename logfilename");
  config_filename = static_cast<string>(argv[1]);
  pct_filename = static_cast<string>(argv[2]);
  out_filename = static_cast<string>(argv[3]);
  log_filename = static_cast<string>(argv[4]);
  //open outfiles and stream
  Utils::FileOpen(out_stream, out_filename);
  //open log file
  Utils::LogFileOpen(log_filename);
  outstring = kTag + "Begining Execution\n";
  outstring += kTag + Utils::TimeCall("begining");
  out_stream << outstring << endl;
  Utils::log_stream << outstring << endl;

  /****************************************************************
   * Add Code Here:
  **/

  outstring = kTag + "outfile '" + out_filename + "'" + "\n";
  outstring += kTag + "logfile '" + log_filename + "'" + "\n";
  outstring += kTag + "configfile '" + config_filename + "'" + "\n";
  outstring += kTag + "pctfile '" + pct_filename + "'" + "\n";
  out_stream << outstring << endl;
  Utils::log_stream << outstring << endl;


  ////////////////////////////////////////////////////////////////////
  // config has RN seed, station count spread, election day length
  //   and mean and dev voting time
  config_stream.OpenFile(config_filename);
  config.ReadConfiguration(config_stream);
  config_stream.Close();


  outstring = kTag + config.ToString() + "\n";
  out_stream << outstring << endl;
  Utils::log_stream << outstring << endl;

  random = MyRandom(config.seed_);


  ////////////////////////////////////////////////////////////////////
  // now read the precinct data
  pct_stream.OpenFile(pct_filename);
  out_stream << "here" << endl;
  simulation.ReadPrecincts(pct_stream);
  pct_stream.Close();

  ////////////////////////////////////////////////////////////////////
  // here is the real work
  simulation.RunSimulation(config, random, out_stream);

  ////////////////////////////////////////////////////////////////////
  // close up and go home
  outstring = kTag + "Ending execution" + "\n";
  outstring += kTag + Utils::TimeCall("ending");
  out_stream << outstring << endl;
  Utils::log_stream << outstring << endl;

  Utils::FileClose(out_stream);
  Utils::FileClose(Utils::log_stream);

  cout<< kTag << "Ending execution" << endl;

  return 0;
}
/****************************************************************
 * End 'main.cc'
**/

