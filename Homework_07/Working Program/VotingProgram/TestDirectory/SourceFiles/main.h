/****************************************************************
 * 'main.h'
 * Header file that includes the 'Utilities' code.
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 12-01-2016--10:20:07
 * THIS IS A TEST FILE CHANGES WILL NOT BE SAVED
 *
**/

#ifndef MAIN_H
#define MAIN_H

#include <iostream>
#include <cmath>
using namespace std;

#include "../../Utilities/utils.h"
#include "../../Utilities/scanner.h"
#include "../../Utilities/scanline.h"
#include "../../Utilities/myrandom.h"

/****************************************************************
 * Add Code Here:
**/

  using namespace std;
  //files included for computation
  #include "configuration.h"
  #include "simulation.h"


#endif // MAIN_H

/****************************************************************
 * End 'main.h'
**/
