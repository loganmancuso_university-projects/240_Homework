#include "simulation.h"
/****************************************************************
 * 'simulation.cc'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 12-01-2016--10:20:07
 * THIS IS A TEST FILE CHANGES WILL NOT BE SAVED
 *
**/

static const string kTag = "SIM: ";

/****************************************************************
* Constructor.
**/
Simulation::Simulation() {
}

/****************************************************************
* Destructor.
**/
Simulation::~Simulation() {
}

/****************************************************************
* Accessors and mutators.
**/

/****************************************************************
* General functions.
**/
/****************************************************************
 * Function 'ReadPrecincts' 
 * @param scanner class for a filestream to the function
 * this function will read the lines of the infile and
 * construct a new instance of a precinct and pass the data to 
 * the newly created precint to start the simulation.
**/
void Simulation::ReadPrecincts(Scanner& infile) {
  while (infile.HasNext()) {
    OnePct new_pct;
    new_pct.ReadData(infile);
    pcts_[new_pct.GetPctNumber()] = new_pct;
  } // while (infile.HasNext()) {
} // void Simulation::ReadPrecincts(Scanner& infile) {

/****************************************************************
 * Function 'RunSimulation' 
 * @param configuration class type config a random generator 
 * random and an outstream to an output file
 * this function will iterate through the map of precints 
 * to find the values assigned to the precint from the information
 * passed to it from the function readprecints (above)
 * from this it will construct an output to push to the outstream
 * file passed into the function. 
**/
void Simulation::RunSimulation(const Configuration& config,
MyRandom& random, ofstream& out_stream) {
  string outstring = "XX";
  int pct_count_this_batch = 0;
  for(auto iterPct = pcts_.begin();
    iterPct != pcts_.end(); ++iterPct) {
    OnePct pct = iterPct->second;

    int expected_voters = pct.GetExpectedVoters();
    if ((expected_voters <=  config.min_expected_to_simulate_) ||
    (expected_voters >   config.max_expected_to_simulate_)) continue;

    outstring = kTag + "RunSimulation for pct " + "\n";
    outstring += kTag + pct.ToString() + "\n";
    Utils::Output(outstring, out_stream); //, Utils::log_stream);

    ++pct_count_this_batch;
    pct.RunSimulationPct(config, random, out_stream);

    //    break; // we only run one pct right now
  } // for(auto iterPct = pcts_.begin(); iterPct != pcts_.end(); ++iterPct)

  outstring = kTag + "PRECINCT COUNT THIS BATCH "
  + Utils::Format(pct_count_this_batch, 4) + "\n";
  Utils::Output(outstring, out_stream);
//  Utils::Output(outstring, out_stream, Utils::log_stream);
//    out_stream << outstring << "HERE" << endl;
//    out_stream.flush();
  //  Utils::log_stream << outstring << endl;
  //  Utils::log_stream.flush();

} // void Simulation::RunSimulation()

/****************************************************************
* Function 'ToString'.
* this function will return a string with the proper formatting
* associated with this classes output information. 
**/
string Simulation::ToString() {
  string s = "";

  for(auto iterPct = pcts_.begin(); 
    iterPct != pcts_.end(); ++iterPct) {
    s += kTag + (iterPct->second).ToString() + "\n";
  }
  return s;
  } // string Simulation::ToString()


/****************************************************************
 * End 'simulation.cc'
**/
