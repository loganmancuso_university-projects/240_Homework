/****************************************************************
 * 'simulation.h'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 12-01-2016--10:20:07
 * THIS IS A TEST FILE CHANGES WILL NOT BE SAVED
 *
**/

#ifndef SIMULATION_H
#define SIMULATION_H

#include <map>

#include "../../Utilities/utils.h"
#include "../../Utilities/scanner.h"
#include "../../Utilities/scanline.h"

using namespace std;

#include "configuration.h"
#include "onepct.h"

class Simulation
{
public:
/****************************************************************
 * Constructors and destructors for the class. 
**/
  Simulation();
  virtual ~Simulation();

/****************************************************************
 * General functions.
**/
  void ReadPrecincts(Scanner& infile);
  void RunSimulation(const Configuration& config,
                     MyRandom& random, ofstream& out_stream);
  string ToString();
  string ToStringPcts();

private:
/****************************************************************
 * Variables.
**/
  map<int, OnePct> pcts_;

/****************************************************************
 * Private functions.
**/
};

#endif // SIMULATION_H




/****************************************************************
 * End 'simulation.h'
**/
