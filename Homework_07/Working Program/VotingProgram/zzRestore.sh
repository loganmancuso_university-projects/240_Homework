# *************************************************************
# 'zzRestore.sh'
# This program restore the 'BackupDirectory' to the
# 'WorkingDirectory' if there is some sort of issue
# with the 'WorkingDirectory'
#
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 12-01-2016--10:20:07
# *************************************************************

#!/bin/bash

# *************************************************************
# Function 'backup'
# move working to WorkingDirectory.save.gz
# file in 'BackupDirectory', incase this is ran by accident
# *************************************************************
backup() {
  cd WorkingDirectory/
  echo "Backing up old 'WorkingDirectory' directory"
  for item in *
  do
    echo "Descend into directory" $item
    cd $item
    echo "Return from directory" $item
    cd ..
    echo "tar directory" $item
    tar -cvf tarfile$item $item
    echo "gzip directory" $item
    gzip tarfile$item
    mv tarfile$item.gz save.tarfile$item.gz
    echo "cp gzipped.save file to 'BackupDirectory'"
    cp save.tarfile$item.gz ../BackupDirectory
    echo "rm the gzipped file from " $item
    rm save.tarfile$item.gz
  done
  echo "Return from 'WorkingDirectory' directory"
  cd ..
}
# *************************************************************
# Function 'restore'
# take old backup from 'BackupDirectory' and restore to
# 'WorkingDirectory'
# *************************************************************
restore(){
  # start the replacement
  echo "Remove the 'WorkingDirectory' directory"
  rm -r WorkingDirectory
  echo "Create a new 'WorkingDirectory' directory"
  mkdir WorkingDirectory
  echo "cp last backup from 'BackupDirectory' into 'WorkingDirectory'"
  cp BackupDirectory/tarfile*.gz WorkingDirectory
  echo "Descend into 'WorkingDirectory' directory"
  cd WorkingDirectory
  for item in *
  do
    gunzip $item
  done
  for item in *
  do
    echo "untar directory" $item
    tar -xvf $item
  done
#
  echo "Remove the tar files"
  rm tarfile*.gz
#
  echo "Return from 'WorkingDirectory' directory"
  cd ..
}

echo "Are You Sure, This Will Remove 'WorkingDirectory' "
select yn in "Yes" "No"; do
  case $yn in
    Yes )
      # backup files from 'WorkingDirectory'
      backup
      # restore old backup to 'WorkingDirectory'
      restore
      break;;
    No )
      echo "Program Terminated"
      break;;
  esac
done


# *************************************************************
# End 'zzRestore.sh'
# *************************************************************

